import { Component, ElementRef, ViewChild } from '@angular/core';
import * as faceapi from 'face-api.js';

@Component({
  selector: 'app-webcam',
  templateUrl: './webcam.component.html',
  styleUrls: ['./webcam.component.scss']
})
export class WebcamComponent {
  WIDTH = 440;
  HEIGHT = 280;

  faceDirection:string = '';
  @ViewChild('video', { static: true }) videoRef?: ElementRef<HTMLVideoElement>;
  @ViewChild('canvas', { static: true }) canvasRef?: ElementRef<HTMLCanvasElement>;

  videoElement!: HTMLVideoElement;
  canvasElement!: HTMLCanvasElement;
  displaySize?: { width: number; height: number };

  async ngOnInit() {
    await Promise.all([
      faceapi.nets.tinyFaceDetector.loadFromUri('./../../assets/models'),
      faceapi.nets.faceLandmark68Net.loadFromUri('./../../assets/models'),
      faceapi.nets.faceRecognitionNet.loadFromUri('./../../assets/models'),
      faceapi.nets.faceExpressionNet.loadFromUri('./../../assets/models')
    ]);
    this.startVideo();
  }

  startVideo() {
    this.videoElement = this.videoRef!.nativeElement;
    navigator.mediaDevices.getUserMedia({ video: {} }).then(stream => {
      this.videoElement.srcObject = stream;
      this.detectFaces();
    }).catch(err => console.log(err));
  }

  async detectFaces() {
    this.videoElement.addEventListener('play', async () => {
      this.canvasElement = faceapi.createCanvasFromMedia(this.videoElement);
      this.canvasRef!.nativeElement.appendChild(this.canvasElement);
      this.displaySize = { width: this.WIDTH, height: this.HEIGHT };
      faceapi.matchDimensions(this.canvasElement, this.displaySize);
      
      setInterval(async () => {
        const detections = await faceapi.detectAllFaces(this.videoElement, new faceapi.TinyFaceDetectorOptions()).withFaceLandmarks();
        const resizedDetections = faceapi.resizeResults(detections, this.displaySize!);
        
        const context = this.canvasElement.getContext('2d');
        if (context) {
          context.clearRect(0, 0, this.canvasElement.width, this.canvasElement.height);
          faceapi.draw.drawDetections(this.canvasElement, resizedDetections);
          faceapi.draw.drawFaceLandmarks(this.canvasElement, resizedDetections);

          if (resizedDetections.length > 0) {
            this.analyzeHeadOrientation(resizedDetections[0].landmarks);
          }
        }
      }, 100);
    });
  }

  analyzeHeadOrientation(landmarks: faceapi.FaceLandmarks68) {
    const nose = landmarks.getNose();
    const leftEye = landmarks.getLeftEye();
    const rightEye = landmarks.getRightEye();

    const noseMidX = nose[3].x;
    const leftEyeX = leftEye[0].x;
    const rightEyeX = rightEye[3].x;

    if (noseMidX < leftEyeX) {
      console.log('right');
      this.faceDirection = 'Right';
    } else if (noseMidX > rightEyeX) {
      console.log('left');
      this.faceDirection = 'Left';
    } else {
      console.log('front');
      this.faceDirection = 'Front';
    }
  }
}
